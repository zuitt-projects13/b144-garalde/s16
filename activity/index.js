let num = 0;

num = Number(prompt('Enter a number: '));

console.log('The number you provided is: ' + num)

for (let i = num; i > 0; i--) {

	if (i <= 50){
		console.log('The current value is at 50. Terminating the loop.')
		break;
	}


	if (i%10 === 0){
		console.log('The number is divisible by 10. Skipping the number ')
		continue
	}

	if (i%5 === 0){
		console.log(i)
		continue
	}

}



let myString = 'supercalifragilisticexpialidocious'

let consonants="";

for (let x = 0; x < myString.length; x++) {
	if ( myString[x] === 'a' || myString[x] === 'e'|| myString[x] === 'i'|| myString[x] === 'o'|| myString[x] === 'u'){
		continue;
	}

	else {
		consonants = consonants + myString[x];
	}

}

console.log(myString)
console.log(consonants)