// WHILE Loop will run the codes while the condition is true

/*
Syntax
	While (conditon) {
		statement;
	}

*/

let count = 5;

while (count !== 0) {
	console.log('While loop: ' + count)
	count--;
}


count = 1

while (count <= 10) {

	console.log(count)
	count++
}



// Do WHile loop execute the code once then check the condition
/*
	syntax

	do {
	
	} While (conditon)

*/

let countThree = 5;

do {
	console.log('Do-While loop' + countThree)
	countThree--;
} while (countThree > 0)



countThree = 1;

do {
	console.log('Do-While loop activity: ' + countThree)
	countThree++;
} while (countThree <= 10)


// For loop 

/*
syntax:

for (initialize, condition, finalexpression) {
	**final expressionis increment or decrement
	statement;
}

*/


for (let countFive = 5; countFive > 0; countFive--) {
	console.log('For Loop: ' + countFive);
}


// let number = Number(prompt('Give me a number:'))

// for (let numCount = 1; numCount <= number ; numCount++ ){
// 	console.log('Hello Batch 144')
// }


let myString = 'Alex';
console.log(myString.length)


console.log(myString[2])


for (let x = 0; x < myString.length; x++) {
	console.log(myString[x])
}



let myName = "Alex"


for (let i=0; i < myName.length; i++) {
	if ( myName[i].toLowerCase() ==='a' || myName[i].toLowerCase() ==='e' || myName[i].toLowerCase() ==='i' || myName[i].toLowerCase() ==='a' || myName[i].toLowerCase() ==='u')

	{
		// if the letter in the name is a vowel it will print 3
		console.log(3)
	}

	else {
		// will print in the console if character is a non-vowel
		console.log(myName[i])
	}


}


/*continue and break
	continue - allows the code to go to the next iteration of the loop 
	break use to terminate the current loop once a match has been found 


*/


for (let count = 0; count<=20;count++) {
	// if remainder is equal to 0
 	if (count%2 === 0) {
 		continue;
 	}
 	// the current value of the number is printed out  if the remainder is not equal to 0;
 	console.log('Continue and break: ' + count)

 	if (count > 10) {
 		break;
 	}
}


let name = 'Alexandro'

for (let i = 0; i < name.length; i++) {
	console.log(name[i]);

	if (name[i].toLowerCase() === 'a') {
		console.log ('Continue to the next iteration')
		continue;
	}

	if (name[i].toLowerCase() === 'd') {
		break;
	}


}